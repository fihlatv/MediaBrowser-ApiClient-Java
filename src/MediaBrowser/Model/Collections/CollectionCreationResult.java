package MediaBrowser.Model.Collections;

public class CollectionCreationResult
{
	private String privateId;
	public final String getId()
	{
		return privateId;
	}
	public final void setId(String value)
	{
		privateId = value;
	}
}