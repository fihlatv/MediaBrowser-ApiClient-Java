package MediaBrowser.Model.Entities;

public final class CollectionType
{
	public static final String Movies = "movies";

	public static final String TvShows = "tvshows";

	public static final String Music = "music";

	public static final String MusicVideos = "musicvideos";

	public static final String Trailers = "trailers";

	public static final String HomeVideos = "homevideos";

	public static final String BoxSets = "boxsets";

	public static final String AdultVideos = "adultvideos";

	public static final String Books = "books";
	public static final String Photos = "photos";
	public static final String Games = "games";
	public static final String Channels = "channels";
	public static final String LiveTv = "livetv";
	public static final String Playlists = "playlists";
	public static final String Folders = "folders";

	public static final String LiveTvNowPlaying = "LiveTvNowPlaying";
	public static final String LiveTvChannels = "LiveTvChannels";
	public static final String LiveTvRecordingGroups = "LiveTvRecordingGroups";

	public static final String TvShowSeries = "TvShowSeries";
	public static final String TvGenres = "TvGenres";
	public static final String TvLatest = "TvLatest";
	public static final String TvNextUp = "TvNextUp";
	public static final String TvResume = "TvResume";
	public static final String TvFavoriteSeries = "TvFavoriteSeries";
	public static final String TvFavoriteEpisodes = "TvFavoriteEpisodes";

	public static final String MovieLatest = "MovieLatest";
	public static final String MovieResume = "MovieResume";
	public static final String MovieMovies = "MovieMovies";
	public static final String MovieCollections = "MovieCollections";
	public static final String MovieFavorites = "MovieFavorites";
	public static final String MovieGenres = "MovieGenres";

	public static final String LatestGames = "LatestGames";
	public static final String RecentlyPlayedGames = "RecentlyPlayedGames";
	public static final String GameSystems = "GameSystems";
	public static final String GameGenres = "GameGenres";
	public static final String GameFavorites = "GameFavorites";

	public static final String MusicArtists = "MusicArtists";
	public static final String MusicAlbumArtists = "MusicAlbumArtists";
	public static final String MusicAlbums = "MusicAlbums";
	public static final String MusicGenres = "MusicGenres";
	public static final String MusicLatest = "MusicLatest";
	public static final String MusicSongs = "MusicSongs";
	public static final String MusicFavorites = "MusicFavorites";
	public static final String MusicFavoriteArtists = "MusicFavoriteArtists";
	public static final String MusicFavoriteAlbums = "MusicFavoriteAlbums";
	public static final String MusicFavoriteSongs = "MusicFavoriteSongs";
}